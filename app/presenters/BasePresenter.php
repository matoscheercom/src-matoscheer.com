<?php

/**
 * Base presenter
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter {

    /** @var Mailer */
    protected $mailer;

    /** @persistent */
    public $lang = 'sk';

    /**
     * @param Mailer $mailer
     */
    public function injectMailer(Mailer $mailer) {
        $this->mailer = $mailer;
    }



    /**
     * @return ContactForm
     */
    public function createComponentContactForm() {
        $c = new ContactForm();
        $c->injectMailer($this->mailer);
        return $c;
    }




    public function beforeRender() {
        parent::beforeRender();

        switch($this->lang) {
            case 'sk':
                $this->template->lang = $this->lang;
                break;
            default:
                $this->redirect('this', array('lang' => 'sk'));
                $this->terminate();
        }
        
        $tm = $this->context->getParameters();
        $this->template->head = $tm['head'][$this->lang];
    }
}
