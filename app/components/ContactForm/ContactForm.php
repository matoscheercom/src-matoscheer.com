<?php

use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Mail\Message;

/**
 * Contact Form component
 * @author Michal Bystricky <michal@fatchilli.com>
 * @author Martin Scheer <martin.scheer@fatchilli.com>
 *
 */
class ContactForm extends Control {

    /** @var Mailer */
    private $mailer;


    /**
     * Mailer injector
     * @param Mailer $mailer
     */
    public function injectMailer(Mailer $mailer) {
        $this->mailer = $mailer;
    }

    /**
     * @return Form
     */
    public function createComponentContactForm() {
        $f = new Form();

        $tm = $this->presenter->context->getParameters();

        $f->addSubmit('submit', "Odoslať");
        $f->addText('name', "Meno a Priezvisko*")
            ->setAttribute('placeholder', "Meno a Priezvisko")
            ->setRequired('Nezadali ste meno.');

        $f->addText('email', "E-mail*")
            ->setAttribute('placeholder', "vas@email.com")
            ->addRule(Form::EMAIL, 'Nesprávne zadaná e-mailová adresa.')
            ->setRequired('Nesprávne zadaná e-mailová adresa.');

        $f->addText('phone', "Telefón:")
            ->setAttribute('placeholder', "Telefón*");




        $f->addText("msg", "Message");

        $f->onSuccess[] = $this->contactFormSubmitted;
        $f->onError[] = $this->refresh;
        return $f;
    }

    public function refresh() {
        $this->redrawControl();
    }

    /**
     * @param \Nette\Application\UI\Form $f
     */
    public function contactFormSubmitted(Form $f) {
        $v = $f->getValues();

        if (!trim($v->msg) ) {

            $params = $this->presenter->context->getParameters();
            $emailAddress = $params['mail']['contactForm'];

            $body = "Na stránke Klepapp.com bol vyplnený kontaktný formulár.\n\n"
                . "Meno a priezvisko: " . $v->name . "\n"
                . "E-mail: " . $v->email . "\n"
                . "Mobil: " . $v->phone . "\n";

            $message = new Message;
            $message
                ->setSubject("Kontaktný formulár")
                ->addTo($emailAddress)
                ->setBody($body);

            $this->mailer->sendMessage($message);
        }

        $this->template->emailSent = true;

        $this->redrawControl();
        $this->presenter->redrawControl();
    }

    public function render() {
        $tm = $this->presenter->context->getParameters();
//        $this->template->contact = $tm['contact'][$this->presenter->lang];
        $this->template->lang = $this->presenter->lang;

        $this->template->setFile(__DIR__ . '/contactForm.latte');
        $this->template->render();
    }

}