(function() {
    App = function() {


        var startAnimations = function(element) {
          document.getElementById(element).classList.add('animate');
        };

        return {
            init: function() {
                setTimeout(startAnimations('ANIMATION'),1500);
            }
        }
    }();

    App.init();

})();