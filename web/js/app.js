setTimeout(function() {
    document.getElementById('animation').classList.add('animate');
},500);


var layoutBtn = document.getElementById('js-layout');
var isAnimation = false;


layoutBtn.onclick = function(event) {
    event.preventDefault();
    var body = document.body;
    var buttonText = layoutBtn.innerHTML;
    if (!isAnimation) {
        isAnimation = true;
        body.classList.toggle('dark');
        body.classList.toggle('light');
        layoutBtn.classList.toggle('active');
        if (buttonText == 'light') {
            var res = 'dark';
        }
        else {
            var res = 'light';
        }
        layoutBtn.innerHTML = res;
        setTimeout(function() {
            isAnimation = false;
        },500);
    }
};
